#!/usr/bin/env make -f

SETUP.PY = ./setup.py
PACKAGE_FOLDER = $(shell python3 -c 'from setuptools import find_packages;print(find_packages(exclude=["tests"])[0])')
DOCS_FOLDER = docs
DOCS_API_FOLDER = docs/source/api
VERSION.PY = $(shell find $(PACKAGE_FOLDER) -type f -name 'version.py')
RPM_CHANGELOG = RPM-CHANGELOG

# get version from __init__.py
VERSION = $(shell perl -ne 'if (s/^.*__version__\s*=\s*"(\d+\.\d+.\d+)".*$$/$$1/g){print;exit}' $(VERSION.PY))

.PHONY: all
all: coverage docs

.PHONY: docs
docs:
	cd $(DOCS_FOLDER) && make html
	-xdg-open $(DOCS_FOLDER)/_build/html/index.html

.PHONY: coverage
coverage:
	coverage run --source=$(PACKAGE_FOLDER) $(SETUP.PY) test
	coverage report
	coverage html
	-xdg-open htmlcov/index.html


.PHONY: $(RPM_CHANGELOG)
$(RPM_CHANGELOG):
	LANG=C git tag -l 'v*' -n99 --sort=-version:refname \
		--format='* %(creatordate:format:%a %b %d %Y) %(taggername) %(taggeremail) - %(refname:strip=2)%0a%(contents)%0a' \
		| perl -pe 's/(\s+)v([\d.]+)/$$1$$2/g' \
		| perl -ne 'print if not m/^\s*$$/g' \
		| perl -pe 's/^(?!\*)\s*(?!-)\s*/- /g' \
		> $@

.PHONY: rpm-sfos
rpm-sfos: $(RPM_CHANGELOG)
	ln -rsf rpm/setup-sfos.cfg setup.cfg
	$(SETUP.PY) bdist_rpm --no-autoreq --changelog="`cat $(RPM_CHANGELOG)`"
	rm -f setup.cfg


.PHONY: clean
clean:
	rm -rf *.egg-info
	rm -rf build
	rm -rf $$(find -type d -iname '__pycache__')
	rm -f $$(find -type f -iname '*.pyc')
	rm -f $(CHANGELOG_RPM)
	(cd $(DOCS_FOLDER) && make clean)
