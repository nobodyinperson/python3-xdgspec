
Installation
============

:mod:`xdgspec` is best installed via :mod:`pip`::

    python3 -m pip install --user xdgspec
