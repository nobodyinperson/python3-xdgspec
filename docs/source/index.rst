Welcome to xdgspec's documentation!
===========================================

:mod:`xdgspec` is a Python package for convenient access to the `XDG Base
Directory Specification <https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html>`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   api/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
