# system modules
import glob
import os
import shutil
import sys
import tempfile
import unittest
from unittest.mock import patch

# internal modules
from xdgspec import *

# external modules


class XDGTest(unittest.TestCase):
    pass


class XDGDirectoryVariableTest(XDGTest):
    def test_invalid_name_raises_ValueError(self):
        with self.assertRaises(ValueError):
            XDGDirectoryVariable("bogus")

    @patch.dict("os.environ", {k: "" for k in XDG_DEFAULTS})
    def test_value_returns_default_if_envvar_empty(self):
        for var, default in XDG_DEFAULTS.items():
            self.assertEqual(
                XDGDirectoryVariable(var).value,
                default,
                "{} does not use the default value {} when the environment "
                "variable {} is empty".format(
                    XDGDirectoryVariable.__name__, default, var
                ),
            )

    @patch.dict(
        "os.environ", {k: "strange-" + k + "-content" for k in XDG_DEFAULTS}
    )
    def test_value_returns_envvar_value(self):
        for var, default in XDG_DEFAULTS.items():
            v = XDGDirectoryVariable(var)
            self.assertEqual(
                v.value,
                os.environ.get(var),
                "{} does not use the value of the environment "
                "variable {} ({})".format(
                    XDGDirectoryVariable.__name__, var, os.environ.get(var)
                ),
            )
            self.assertEqual(str(v), v.value)


class DirectoryTest(XDGTest):
    def test_usage_as_context_manager_creates_nonexistent_dir(self):
        with tempfile.TemporaryDirectory() as empty_dir:
            d = Directory(
                os.path.join(empty_dir, sys._getframe().f_code.co_name)
            )
            with d as created_dir:
                self.assertTrue(
                    os.path.exists(created_dir),
                    "Using {} as context manager did not create a "
                    "nonexisting directory".format(type(d).__name__),
                )

    def test_stringification_returns_path(self):
        for s in ("asdf", ".fdsa", "/home/user", "~/internet"):
            with self.subTest(path=s):
                d = Directory(s)
                self.assertEqual(str(d), s)


class XDGDirectoryTest(XDGTest):
    @patch.dict(
        "os.environ", {k: "~/.other-{}".format(k) for k in XDG_HOME_DEFAULTS}
    )
    def test_path_expands_tilde(self):
        for var, default in XDG_HOME_DEFAULTS.items():
            with self.subTest(variable=var):
                d = XDGDirectory(var)
                self.assertEqual(
                    d.path,
                    os.path.expanduser(os.environ.get(var)),
                    "{}.path does not expand the tilde in the value of the "
                    " environment variable {} ({})".format(
                        XDGDirectory.__name__, var, os.environ.get(var)
                    ),
                )
                self.assertEqual(str(d), d.path)


class XDGPackageDirectoryTest(XDGTest):
    @patch.dict(
        "os.environ", {k: "some-value".format(k) for k in XDG_HOME_DEFAULTS}
    )
    def test_path_appends_packagename(self):
        for var, default in XDG_HOME_DEFAULTS.items():
            with self.subTest(variable=var):
                d = XDGPackageDirectory(var, packagename="xdgspec")
                self.assertEqual(
                    d.path,
                    os.path.join(
                        os.path.expanduser(os.environ.get(var)),
                        xdgspec.__name__,
                    ),
                    "{}.path does not append the package name".format(
                        XDGPackageDirectory.__name__, var, os.environ.get(var)
                    ),
                )
                self.assertEqual(str(d), d.path)


class XDGDirectoriesTest(XDGTest):
    @patch.dict(
        "os.environ", {k: ":".join([k] * 4) for k in XDG_DIRS_DEFAULTS}
    )
    def test_paths_splits_environment_variable_content(self):
        for var, default in XDG_DIRS_DEFAULTS.items():
            self.assertEqual(
                XDGDirectories(var).paths,
                os.environ.get(var).split(":"),
                "{}.paths does not split the "
                "environment variable content".format(XDGDirectories.__name__),
            )

    @patch.dict(
        "os.environ",
        {
            k: ":".join(
                (list(glob.glob("*/")) + [".....nonexisting....."]) * 2
            )
            for k in XDG_DIRS_DEFAULTS
        },
    )
    def test_iterating_yields_unique_existing_paths(self):
        for var, default in XDG_DIRS_DEFAULTS.items():
            self.assertEqual(
                list(XDGDirectories(var)),
                list(glob.glob("*/")),
                "Iterating over {} does not yield only existing and "
                "unique directories".format(XDGDirectories.__name__),
            )
